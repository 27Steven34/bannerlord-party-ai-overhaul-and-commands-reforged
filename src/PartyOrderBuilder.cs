﻿using HarmonyLib;
using PartyAIOverhaulCommands.src.Behaviours;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.Actions;
using TaleWorlds.CampaignSystem.ViewModelCollection;
using TaleWorlds.Core;
using TaleWorlds.Library;
using TaleWorlds.Localization;
using TaleWorlds.MountAndBlade;

namespace PartyAIOverhaulCommands
{
    partial class PartyOrder
    {
        partial class PartyOrderBuilder
        {
            public static readonly PartyOrderBuilder Instance = new PartyOrderBuilder();

            private int line_index = 0;
            private readonly int lines_per_page = 8;
            private int lines_current_page = 1;

            private bool findplayerifbored = false;

            private PartyOrder order = null;
            private ConversationManager conversation_manager = null;
            private CampaignGameStarter cgs = null;
            public void RegisterDialogue(CampaignGameStarter campaignGameStarter)
            {
                try
                {
                    cgs = campaignGameStarter;
                    campaignGameStarter.AddPlayerLine("give_party_order", "hero_main_options", "give_party_order_reply", "{=mZEVvOi4}I have a new assignment for you."
                        , new ConversationSentence.OnConditionDelegate(this.conversation_is_clan_party_on_condition), () => { lines_current_page = 1; order = new PartyOrder(Hero.OneToOneConversationHero); }, 96, null, null);
                    campaignGameStarter.AddDialogLine("give_party_order_reply", "give_party_order_reply", "give_party_order_choose", "{=GVM3O4PW}What is it?", null, null, 100, null);
                    campaignGameStarter.AddPlayerLine("give_party_order_patrol", "give_party_order_choose", "give_party_order_reply2", "{=sFk6o6pM}I'm sending you on a patrol.", null, null, 99, null, null);
                    campaignGameStarter.AddPlayerLine("give_party_order_patrol_nevermind", "give_party_order_choose", "lord_pretalk", "{=IN6LdIzh}Never mind.", null, null, 99);
                    campaignGameStarter.AddDialogLine("give_party_order_reply2", "give_party_order_reply2", "give_party_order_patrol_choose", "{=3LDRNP3W}Where should I patrol?", null, () => { line_index = 0; }, 100, null);

                    IEnumerable<Settlement> settlements = Campaign.Current.Settlements;
                    foreach (Settlement settlement in settlements)
                    {
                        int priority = (settlement.OwnerClan == Hero.MainHero.Clan ? 101 : 100);
                        campaignGameStarter.AddPlayerLine("give_party_order_patrol_" + settlement.Name.ToString(), "give_party_order_patrol_choose", "give_party_order_patrol_ask_additional", "{=xa1kmmj5}" + settlement.Name.ToString()
                            , () => { return get_is_line_on_page(settlement.MapFaction == Hero.MainHero.MapFaction || Campaign.Current.Models.MapDistanceModel.GetDistance(Hero.MainHero.PartyBelongedTo, settlement) < Campaign.AverageDistanceBetweenTwoTowns); }, () => { order.TargetSettlement = settlement; }, priority);
                    }
                    campaignGameStarter.AddPlayerLine("give_party_order_patrol_more", "give_party_order_patrol_choose", "give_party_order_reply2", "{=XRGi3bSd}More...", () => { return (lines_per_page < line_index); }, () => { lines_current_page = (lines_per_page * lines_current_page < line_index) ? lines_current_page + 1 : 1; });

                    campaignGameStarter.AddPlayerLine("give_party_order_patrol_nevermind1", "give_party_order_patrol_choose", "lord_pretalk", "{=gX0L27IB}Never mind.", null, null, 99);
                    campaignGameStarter.AddDialogLine("give_party_order_patrol_ask_additional", "give_party_order_patrol_ask_additional", "give_party_order_patrol_additional", "{ADDITIONAL_ORDERS_PATROL_1}{ADDITIONAL_ORDERS_PATROL_2}{ADDITIONAL_ORDERS_PATROL_3}", () => { return give_party_order_patrol_additional_condition(); }, null, 100, null);

                    campaignGameStarter.AddPlayerLine("give_party_order_order.FriendlyVillagesScoreMultiplier", "give_party_order_patrol_additional", "give_party_order_patrol_ask_additional", "{=yX4VtrOP}Focus on protecting our clan's villages.", () => { return order.FriendlyVillagesScoreMultiplier > 0f; }, () => { order.FriendlyVillagesScoreMultiplier = 0f; });
                    campaignGameStarter.AddPlayerLine("give_party_order_order.FriendlyVillagesScoreMultiplier_revert", "give_party_order_patrol_additional", "give_party_order_patrol_ask_additional", "{=KkjBE78p}On second thought, also come to the aid of villages of allied clans.", () => { return order.FriendlyVillagesScoreMultiplier == 0f; }, () => { order.FriendlyVillagesScoreMultiplier = 1f * order.OwnClanVillagesScoreMultiplier; });

                    campaignGameStarter.AddPlayerLine("give_party_order_patrol_distant_villages", "give_party_order_patrol_additional", "give_party_order_patrol_ask_additional", "{=pmqWOoV0}Also help the more distant villages if they raise the alarm.", () => { return order.OwnClanVillagesScoreMultiplier == 1f; }, () => { order.OwnClanVillagesScoreMultiplier = 1.3f; order.FriendlyVillagesScoreMultiplier *= 1.3f; });
                    campaignGameStarter.AddPlayerLine("give_party_order_patrol_distant_villages_revert", "give_party_order_patrol_additional", "give_party_order_patrol_ask_additional", "{=uHzw3ULH}I changed my mind, do not travel too far to help other villages.", () => { return order.OwnClanVillagesScoreMultiplier > 1f; }, () => { order.OwnClanVillagesScoreMultiplier = 1f; if (order.FriendlyVillagesScoreMultiplier > 0f) { order.FriendlyVillagesScoreMultiplier = 1f; } });

                    campaignGameStarter.AddPlayerLine("give_party_order_order.PartyMaintenanceScoreMultiplierenance", "give_party_order_patrol_additional", "give_party_order_patrol_ask_additional", "{=F2vg9SGK}Visit often the surrounding settlements for your party's needs.", () => { return order.PartyMaintenanceScoreMultiplier == 1f; }, () => { order.PartyMaintenanceScoreMultiplier = 1.7f; });
                    campaignGameStarter.AddPlayerLine("give_party_order_order.PartyMaintenanceScoreMultiplierenance_revert", "give_party_order_patrol_additional", "give_party_order_patrol_ask_additional", "{=16NEiony}Actually, don't visit the surrounding settlements more than usual.", () => { return order.PartyMaintenanceScoreMultiplier > 1f; }, () => { order.PartyMaintenanceScoreMultiplier = 1f; });

                    campaignGameStarter.AddPlayerLine("give_party_order_patrol_confirm", "give_party_order_patrol_additional", "affirm_party_order", "{=AZYIGzy8}That's it. You have your orders.", null, delegate { give_party_order_patrol_confirm_consequence(); });
                    campaignGameStarter.AddPlayerLine("give_party_order_patrol_nevermind2", "give_party_order_patrol_additional", "lord_pretalk", "{=O1M2H0C2}Never mind.", null, null, 99);


                    campaignGameStarter.AddPlayerLine("give_party_order_escort", "give_party_order_choose", "give_party_order_escort_ask_additional", "{=S7IXOvya}Your party is to follow mine.", null, delegate { order._attackInitiative = 1f; order._avoidInitiative = 1f; }, 100, null, null);
                    campaignGameStarter.AddDialogLine("give_party_order_escort_ask_additional", "give_party_order_escort_ask_additional", "give_party_order_escort_additional", "{ADDITIONAL_ORDERS_ESCORT}", () => { return give_party_order_escort_additional_condition(); }, null, 100, null);

                    campaignGameStarter.AddPlayerLine("give_party_order_escort_attack_initiative", "give_party_order_escort_additional", "give_party_order_escort_ask_additional", "{=9VkT0QtD}Do not chase enemies until I give the signal.", () => { return order._attackInitiative == 1f; }, () => { order._attackInitiative = 0f; });
                    campaignGameStarter.AddPlayerLine("give_party_order_escort_attack_initiative_revert", "give_party_order_escort_additional", "give_party_order_escort_ask_additional", "{=KbwF9ipY}On second thought, engage our enemies at will.", () => { return order._attackInitiative == 0f; }, () => { order._attackInitiative = 1f; });

                    campaignGameStarter.AddPlayerLine("give_party_order_escort_avoid_initiative", "give_party_order_escort_additional", "give_party_order_escort_ask_additional", "{=ShLWeZiY}Do not flee from dangerous enemies.", () => { return order._avoidInitiative == 1f; }, () => { order._avoidInitiative = 0f; });
                    campaignGameStarter.AddPlayerLine("give_party_order_escort_avoid_initiative_revert", "give_party_order_escort_additional", "give_party_order_escort_ask_additional", "{=jdgK44KM}Actually, avoid dangerous enemies as you would otherwise", () => { return order._avoidInitiative == 0f; }, () => { order._avoidInitiative = 1f; });

                    campaignGameStarter.AddPlayerLine("give_party_order_escort_stop_imprisoning", "give_party_order_escort_additional", "give_party_order_escort_ask_additional", "{=99JeSjud}Stop taking prisoners, except for the leading nobility.", () => { return !order.StopTakingPrisoners; }, () => { order.StopTakingPrisoners = true; });
                    campaignGameStarter.AddPlayerLine("give_party_order_escort_stop_imprisoning_revert", "give_party_order_escort_additional", "give_party_order_escort_ask_additional", "{=xPRt2fdZ}I've reconsidered, feel free to take prisoners at will.", () => { return order.StopTakingPrisoners; }, () => { order.StopTakingPrisoners = false; });

                    campaignGameStarter.AddPlayerLine("give_party_order_escort_confirm", "give_party_order_escort_additional", "affirm_party_order", "{=IQKXYHAq}That's it. You have your orders.", null, delegate { give_party_order_escort_confirm_consequence(); });
                    campaignGameStarter.AddPlayerLine("give_party_order_escort_nevermind", "give_party_order_escort_additional", "lord_pretalk", "{=6LrwJL3W}Never mind.", null, null, 99);


                    campaignGameStarter.AddPlayerLine("give_party_order_roam", "give_party_order_choose", "give_party_order_roam_ask_additional", "{=5ur2y5hP}I want you to roam the lands.", null, delegate { order._attackInitiative = 1f; order._avoidInitiative = 1f; order.LeaveTroopsToGarrisonOtherClans = true; order.AllowJoiningArmies = true; order.AllowRaidingVillages = true; order.HostileSettlementsScoreMultiplier = 1f; findplayerifbored = false; order.FriendlyVillagesScoreMultiplier = 1f; }, 101, null, null);
                    campaignGameStarter.AddDialogLine("give_party_order_roam_ask_additional", "give_party_order_roam_ask_additional", "give_party_order_roam_additional", "{ADDITIONAL_ORDERS_ROAM_1}{ADDITIONAL_ORDERS_ROAM_2}{ADDITIONAL_ORDERS_ROAM_3}{ADDITIONAL_ORDERS_ROAM_4}{ADDITIONAL_ORDERS_ROAM_5}{ADDITIONAL_ORDERS_ROAM_6}{ADDITIONAL_ORDERS_ROAM_7}{ADDITIONAL_ORDERS_ROAM_8}", () => { return give_party_order_roam_additional_condition(); }, null, 100, null);

                    campaignGameStarter.AddPlayerLine("give_party_order_roam_only_clan", "give_party_order_roam_additional", "give_party_order_roam_ask_additional", "{=hVxWVKX7}Don't bother protecting settlements of allied clans.", () => { return order.FriendlyVillagesScoreMultiplier > 0f; }, () => { order.FriendlyVillagesScoreMultiplier = 0f; });
                    campaignGameStarter.AddPlayerLine("give_party_order_roam_only_clan_revert", "give_party_order_roam_additional", "give_party_order_roam_ask_additional", "{=welScFSY}I've reconsidered, protect settlements of allied clans as well", () => { return order.FriendlyVillagesScoreMultiplier == 0f; }, () => { order.FriendlyVillagesScoreMultiplier = 1f; });

                    campaignGameStarter.AddPlayerLine("give_party_order_roam_leave_garrison", "give_party_order_roam_additional", "give_party_order_roam_ask_additional", "{=7E06qXBJ}Never leave our troops in garrisons of allied clans.", () => { return order.LeaveTroopsToGarrisonOtherClans; }, () => { order.LeaveTroopsToGarrisonOtherClans = false; });
                    campaignGameStarter.AddPlayerLine("give_party_order_roam_leave_garrison_revert", "give_party_order_roam_additional", "give_party_order_roam_ask_additional", "{=wZOn0NEF}Actually, feel free to donate troops to the garrisons of allied clans.", () => { return !order.LeaveTroopsToGarrisonOtherClans; }, () => { order.LeaveTroopsToGarrisonOtherClans = true; });

                    campaignGameStarter.AddPlayerLine("give_party_order_roam_join_army", "give_party_order_roam_additional", "give_party_order_roam_ask_additional", "{=hn1giswY}Do not merge with armies unless I personally lead.", () => { return order.AllowJoiningArmies; }, () => { order.AllowJoiningArmies = false; });
                    campaignGameStarter.AddPlayerLine("give_party_order_roam_join_army_revert", "give_party_order_roam_additional", "give_party_order_roam_ask_additional", "{=ksQCaRwa}I will allow you to merge with armies after all.", () => { return !order.AllowJoiningArmies; }, () => { order.AllowJoiningArmies = true; });

                    campaignGameStarter.AddPlayerLine("give_party_order_roam_no_raiding", "give_party_order_roam_additional", "give_party_order_roam_ask_additional", "{=1EgmZn7s}Refrain from raiding any villages.", () => { return order.AllowRaidingVillages; }, () => { order.AllowRaidingVillages = false; });
                    campaignGameStarter.AddPlayerLine("give_party_order_roam_no_raiding_revert", "give_party_order_roam_additional", "give_party_order_roam_ask_additional", "{=cTY5EsLq}On second thought, I allow you to raid the villages of our enemies.", () => { return !order.AllowRaidingVillages; }, () => { order.AllowRaidingVillages = true; });

                    campaignGameStarter.AddPlayerLine("give_party_order_roam_no_sieging", "give_party_order_roam_additional", "give_party_order_roam_ask_additional", "{=MkcYMAy8}Do not besiege any towns or castles.", () => { return order.HostileSettlementsScoreMultiplier > 0f; }, () => { order.HostileSettlementsScoreMultiplier = 0f; });
                    campaignGameStarter.AddPlayerLine("give_party_order_roam_no_sieging_revert", "give_party_order_roam_additional", "give_party_order_roam_ask_additional", "{=1cnLX765}After reconsideration, you may grasp opportunities to conquer new lands.", () => { return order.HostileSettlementsScoreMultiplier == 0f; }, () => { order.HostileSettlementsScoreMultiplier = 1f; });

                    campaignGameStarter.AddPlayerLine("give_party_order_roam_avoid_initiative", "give_party_order_roam_additional", "give_party_order_roam_ask_additional", "{=Ym9U0Qzi}Avoid enemies unless they are much weaker than you.", () => { return order._avoidInitiative == 1f; }, () => { order._avoidInitiative = 1.1f; });
                    campaignGameStarter.AddPlayerLine("give_party_order_roam_avoid_initiative_revert", "give_party_order_roam_additional", "give_party_order_roam_ask_additional", "{=UFKzmROA}Actually, do not avoid any enemies you may be able to defeat", () => { return order._avoidInitiative > 1f; }, () => { order._avoidInitiative = 1f; });

                    campaignGameStarter.AddPlayerLine("give_party_order_roam_attack_initiative", "give_party_order_roam_additional", "give_party_order_roam_ask_additional", "{=RfBlwhmR}Only chase the most tempting enemy parties.", () => { return order._attackInitiative == 1f; }, () => { order._attackInitiative = 0.9f; });
                    campaignGameStarter.AddPlayerLine("give_party_order_roam_attack_initiative_revert", "give_party_order_roam_additional", "give_party_order_roam_ask_additional", "{=ESFnnRKG}I've reconsidered, attack our enemies as usual.", () => { return order._attackInitiative < 1f; }, () => { order._attackInitiative = 1f; });

                    campaignGameStarter.AddPlayerLine("give_party_order_roam_find_player", "give_party_order_roam_additional", "give_party_order_roam_ask_additional", "{=wlCqbKzm}Come find me whenever you can't think of anything else worth doing.", () => { return !findplayerifbored; }, () => { findplayerifbored = true; });
                    campaignGameStarter.AddPlayerLine("give_party_order_roam_find_player_revert", "give_party_order_roam_additional", "give_party_order_roam_ask_additional", "{=HT5HEB5P}On secound thought, don't bother finding me on your own initiative.", () => { return findplayerifbored; }, () => { findplayerifbored = false; });

                    campaignGameStarter.AddPlayerLine("give_party_order_roam_confirm", "give_party_order_roam_additional", "affirm_party_order", "{=XyzVaqot}That's it. You know what to do.", null, delegate { give_party_order_roam_confirm_consequence(); });
                    campaignGameStarter.AddPlayerLine("give_party_order_roam_nevermind", "give_party_order_roam_additional", "lord_pretalk", "{=GHXpDIEK}Never mind.", null, null, 99);


                    campaignGameStarter.AddDialogLine("affirm_party_order", "affirm_party_order", "close_window", "{=CX6JHsbB}We shall carry out your instructions!", null, new ConversationSentence.OnConsequenceDelegate(conversation_lord_leave_on_consequence), 100, null);


                    campaignGameStarter.AddPlayerLine("equipment_party_clan", "hero_main_options", "equipment_party_clan_reply", "{=9n1Uij0W}Let me see your goods and equipment.", new ConversationSentence.OnConditionDelegate(conversation_is_clan_party_on_condition), new ConversationSentence.OnConsequenceDelegate(this.conversation_equipment_party_clan_on_consequence), 99, null, null);
                    campaignGameStarter.AddDialogLine("equipment_party_clan_reply", "equipment_party_clan_reply", "lord_pretalk", "{=kJC7LLJm}All right.", new ConversationSentence.OnConditionDelegate(conversation_equipment_clan_reply_on_condition), null, 100, null);
                    campaignGameStarter.AddDialogLine("equipment_party_clan_reply", "equipment_party_clan_reply", "lord_pretalk", "{=52AcmMM0}All right, I will change my gear after our conversation.", new ConversationSentence.OnConditionDelegate(conversation_equipment_clan_reply_change_on_condition), () => { battle_equipment_backup = null; civilian_equipment_backup = null; }, 100, null);

                    campaignGameStarter.AddPlayerLine("troops_and_prisoners_party_clan", "hero_main_options", "troops_and_prisoners_party_clan_reply", "{=dob2z0My}Let's exchange troops and prisoners.", new ConversationSentence.OnConditionDelegate(conversation_is_clan_party_on_condition), null, 97, null, null);
                    campaignGameStarter.AddDialogLine("troops_and_prisoners_party_clan_reply", "troops_and_prisoners_party_clan_reply", "lord_pretalk", "{=ps3U3ots}All right.", new ConversationSentence.OnConditionDelegate(this.conversation_troops_and_prisoners_party_clan_on_condition), null, 100, null);

                    campaignGameStarter.AddPlayerLine("equipment_caravan_clan", "caravan_talk", "equipment_caravan_clan_reply", "{=TAyfMDef}Let me see your goods and equipment.", new ConversationSentence.OnConditionDelegate(conversation_is_clan_party_or_caravan_on_condition), new ConversationSentence.OnConsequenceDelegate(this.conversation_equipment_party_clan_on_consequence), 101, null, null);
                    campaignGameStarter.AddDialogLine("equipment_caravan_clan_reply", "equipment_caravan_clan_reply", "caravan_pretalk", "{=1baIw5Rl}All right.", new ConversationSentence.OnConditionDelegate(conversation_equipment_clan_reply_on_condition), null, 100, null);
                    campaignGameStarter.AddDialogLine("equipment_caravan_clan_reply_change", "equipment_caravan_clan_reply", "caravan_pretalk", "{=dQwm7RgG}All right, I will change my equipment after our conversation.", new ConversationSentence.OnConditionDelegate(conversation_equipment_clan_reply_change_on_condition), () => { battle_equipment_backup = null; civilian_equipment_backup = null; }, 100, null);

                    campaignGameStarter.AddPlayerLine("troops_and_prisoners_caravan_clan", "caravan_talk", "troops_and_prisoners_caravan_clan_reply", "{=iGMVomiK}Let's exchange troops and prisoners.", new ConversationSentence.OnConditionDelegate(conversation_is_clan_party_or_caravan_on_condition), null, 102, null, null);
                    campaignGameStarter.AddDialogLine("troops_and_prisoners_caravan_clan_reply", "troops_and_prisoners_caravan_clan_reply", "caravan_pretalk", "{=4RvBh0oa}All right.", new ConversationSentence.OnConditionDelegate(this.conversation_troops_and_prisoners_party_clan_on_condition), null, 100, null);


                    campaignGameStarter.AddPlayerLine("give_party_order_disband_join", "give_party_order_choose", "give_party_order_disband_join_ask_additional", "{=KQwXgzec}I want you and your entire party to merge into mine.", null, null, 103, null, null);
                    campaignGameStarter.AddDialogLine("give_party_order_disband_join_ask_additional", "give_party_order_disband_join_ask_additional", "give_party_order_disband_join_additional", "{=2zCnqIKP}Are you sure?", null, null, 100, null);

                    campaignGameStarter.AddPlayerLine("give_party_order_disband_join_confirm", "give_party_order_disband_join_additional", "close_window", "{=e4bQb6Sj}Yes, I'm sure.", null, delegate { PlayerEncounter.LeaveEncounter = true; MergeDisbandParty(Hero.OneToOneConversationHero.PartyBelongedTo, MobileParty.MainParty.Party); });
                    campaignGameStarter.AddPlayerLine("give_party_order_disband_join_nevermind", "give_party_order_disband_join_additional", "lord_pretalk", "{=hzBp0Sdu}Never mind.", null, null, 99);

                    campaignGameStarter.AddPlayerLine("cancel_party_order", "hero_main_options", "cancel_party_order_reply", "{=L6eNTxsS}All your standing orders are hereby rescinded.", () => { return Hero.OneToOneConversationHero.getOrder() != null; }, null, 97);
                    campaignGameStarter.AddDialogLine("cancel_party_order_reply_affirm", "cancel_party_order_reply", "lord_pretalk", "{=6O44WwxR}All right.", null, delegate { Hero.OneToOneConversationHero.cancelOrder(); }, 100, null);

                    campaignGameStarter.AddPlayerLine("give_party_order_army_join", "give_party_order_choose", "give_party_order_army_join_ask_additional", "{=2zoK2gG3}I want your party in my army.", null, null, 102, null, null);
                    campaignGameStarter.AddDialogLine("give_party_order_army_join_ask_additional", "give_party_order_army_join_ask_additional", "give_party_order_army_join_additional", "{=zAZKsHor}Are you sure?", null, null, 100, null);

                    campaignGameStarter.AddPlayerLine("give_party_order_army_join_confirm", "give_party_order_army_join_additional", "close_window", "{=gj7auuZH}Yes, I'm sure.", null, delegate { join_army(); });
                    campaignGameStarter.AddPlayerLine("give_party_order_army_join_nevermind", "give_party_order_army_join_additional", "lord_pretalk", "{=pURcjber}Never mind.", null, null, 99);

                    campaignGameStarter.AddPlayerLine("cancel_all_party_order", "hero_main_options", "cancel_all_party_order_reply", "{=OXdtDXFm}Spread the word, everyone's orders are hereby rescinded.", () => { return Hero.OneToOneConversationHero.Clan == Hero.MainHero.Clan && Config.Value.EnableDebugCancelAllOrders; }, null, 110);
                    campaignGameStarter.AddDialogLine("cancel_all_party_order_reply_affirm", "cancel_all_party_order_reply", "lord_pretalk", "{=6O44WwxR}All right.", null, delegate { PartyAICommandsBehavior.Instance.order_map = new Dictionary<Hero, PartyOrder>(); }, 100, null);

                    campaignGameStarter.AddPlayerLine("recruit_template_party_order", "hero_main_options", "recruit_template_party_order_reply", "{=JRfAvHER}Let's review your party's composition plan.", () => { return conversation_is_clan_party_or_caravan_on_condition(); }, null, 110);
                    campaignGameStarter.AddDialogLine("recruit_template_party_order_reply1", "recruit_template_party_order_reply", "recruit_template_party_order_menu1", "{=GCLIA72h}All right.", null, null, 100, null);

                    campaignGameStarter.AddPlayerLine("recruit_template_party_order_remove", "recruit_template_party_order_menu1", "lord_pretalk", "{=s7s9Ildy}Forget what I said earlier, recruit anyone you like.", () => { return Hero.OneToOneConversationHero.getTemplate() != null; }, delegate { PartyAICommandsBehavior.Instance.template_map.Remove(Hero.OneToOneConversationHero); }, 109);
                    campaignGameStarter.AddPlayerLine("recruit_template_party_order", "recruit_template_party_order_menu1", "recruit_template_party_order_reply1", "{=fkdrQGr6}Let's decide what troops you should recruit.", () => { return conversation_is_clan_party_or_caravan_on_condition(); }, null, 110);
                    campaignGameStarter.AddDialogLine("recruit_template_party_order_reply1", "recruit_template_party_order_reply1", "recruit_template_party_order_reply2", "{=6IpN0iuL}All right, what troop trees will we look at?", null, null, 100, null);
                    campaignGameStarter.AddDialogLine("recruit_template_party_order_reply2", "recruit_template_party_order_reply2", "recruit_template_party_order2", "{=6i4eGhT1}Are there any troops I should exclude or limit to a certain amount?", null, delegate { template_select_troop_trees(); }, 100, null);

                    campaignGameStarter.AddPlayerLine("recruit_template_party_order", "recruit_template_party_order2", "recruit_template_party_order_reply2", "{=bwKblPvf}Wait, let's go back.", null, null, 100);
                    campaignGameStarter.AddPlayerLine("recruit_template_party_order", "recruit_template_party_order2", "recruit_template_party_order_reply_affirm2", "{=7xD1KsX5}Let's have a closer look.", () => { return template_party.MemberRoster.Count > 0 || Hero.OneToOneConversationHero.PartyBelongedTo.Party.NumberOfAllMembers > 1; }, delegate { template_set_troop_limits(); }, 110);
                    campaignGameStarter.AddPlayerLine("recruit_template_party_order_empty", "recruit_template_party_order2", "lord_pretalk", "{=8qD1KsX5}Just don't recruit anyone whatsoever.", () => { return template_party.MemberRoster.Count == 0 && Hero.OneToOneConversationHero.PartyBelongedTo.Party.NumberOfAllMembers == 1; }, delegate { template_apply_and_clean_up(); }, 110);


                    campaignGameStarter.AddDialogLine("recruit_template_party_order_reply_affirm2", "recruit_template_party_order_reply_affirm2", "lord_pretalk", "{=uacVKFj6}All right, I won't recruit any other troops than those and will adhere to any limits you have set.", null, delegate { template_apply_and_clean_up(); }, 100, null);

                    if (PartyAICommandsBehavior.Instance?.template_map == null)
                        PartyAICommandsBehavior.Instance.template_map = new Dictionary<Hero, TroopRoster>();
                    foreach (KeyValuePair<Hero, TroopRoster> pair in PartyAICommandsBehavior.Instance.template_map)
                    {
                        Hero hero = pair.Key;
                        if (hero == null || pair.Value == null)
                            continue;
                        campaignGameStarter.AddPlayerLine("template_" + hero.Name.ToString(), "recruit_template_party_order_menu1", "recruit_template_party_order_reply_affirm3", "{=xa1kmmj5}Use the same plan as " + hero.Name.ToString() + "."
                            , () => { return PartyAICommandsBehavior.Instance.template_map.ContainsKey(hero) && hero != Hero.OneToOneConversationHero; }, () => { template_party = new MobileParty(); template_party.StringId = ""; template_party.MemberRoster.Add(pair.Value); }, 141);
                    }
                    campaignGameStarter.AddDialogLine("recruit_template_party_order_reply_affirm3", "recruit_template_party_order_reply_affirm3", "lord_pretalk", "{=uac43Fj6}All right, I'll make a copy of that composition plan immediately.", null, delegate { template_apply_and_clean_up(); }, 100, null);
                    conversation_manager = Traverse.Create(campaignGameStarter).Field("_conversationManager").GetValue<ConversationManager>();
                }
                catch (Exception e)
                {
                    MessageBox.Show(Utils.FlattenException(e));
                }
            }

            private ConversationSentence getConversationSentenceByID(string id)
            {
                List<ConversationSentence> list = Traverse.Create(conversation_manager).Field("_sentences").GetValue<List<ConversationSentence>>();
                foreach (ConversationSentence sentence in list)
                {
                    if (sentence.Id == id)
                        return sentence;
                }
                return null;
            }

            private void template_apply_and_clean_up()
            {
                Hero hero = Hero.OneToOneConversationHero;
                PartyAICommandsBehavior.RegisterTemplate(hero, template_party.MemberRoster);
                //template_party.RemoveParty();
                if (template_party != null)
                    removeParty(ref template_party);
                if (all_recruits_party != null)
                    removeParty(ref all_recruits_party);
                if (template_limits_party != null)
                    removeParty(ref template_limits_party);
                IsMainTroopsLimitWarningEnabledPatch.ignore = false;
                if (getConversationSentenceByID("template_" + hero.Name.ToString()) == null)
                {
                    cgs.AddPlayerLine("template_" + hero.Name.ToString(), "recruit_template_party_order_menu1", "recruit_template_party_order_reply_affirm3", "{=xa1kmmj5}Use the same plan as " + hero.Name.ToString() + "."
                        , () => { return PartyAICommandsBehavior.Instance.template_map.ContainsKey(hero) && hero != Hero.OneToOneConversationHero; }, () => { template_party = new MobileParty(); template_party.StringId = "";  template_party.MemberRoster.Add(PartyAICommandsBehavior.Instance.template_map[hero]); }, 141);
                    //Traverse.Create<ConversationSentence>().
                    //Traverse.Create(conversation_manager).Method("AddDialogLine", new Type[] { typeof(ConversationSentence) }).GetValue(
                    //new ConversationSentence("template_" + Hero.OneToOneConversationHero.Name.ToString(), new TextObject("{= xa1kmmj5 }Use the same plan as " + Hero.OneToOneConversationHero.Name.ToString() + ".", null), "recruit_template_party_order_menu1", "recruit_template_party_order_reply_affirm3", () => { return PartyAICommandsBehavior.Instance.template_map.ContainsKey(hero); }, null, () => { TroopRoster roster = new TroopRoster(); roster.Add(pair.Value); PartyAICommandsBehavior.Instance.template_map[Hero.OneToOneConversationHero] = roster; }, 1U, 101, 0, 0, null, false, null, null, null));
                }
            }

            private void removeParty(ref MobileParty party)
            {
                if (party != null) 
                { 
                    party.MemberRoster.Reset();
                    party.PrisonRoster.Reset();
                    party.IsActive = false;
                    party.IsVisible = false;
                    Traverse.Create(Campaign.Current.CampaignObjectManager).Method("FinalizeParty", new Type[] { typeof(PartyBase) }).GetValue(party.Party);
                    GC.SuppressFinalize(party.Party);
                    party = null;
                }
            }


            static MobileParty template_party;
            static MobileParty all_recruits_party;
            static MobileParty template_limits_party;
            private void template_select_troop_trees()
            {
                try
                {
                    //TransferTroopPatch.ignore = false;
                    IsMainTroopsLimitWarningEnabledPatch.ignore = false;
                    
                    if (template_party == null)
                    {
                        template_party = new MobileParty();
                        template_party.StringId = "";
                        template_party.SetCustomName(new TextObject("{=6a8ajCJO}Selected Troop Trees"));
                    }
                    if (all_recruits_party == null)
                    {
                        all_recruits_party = new MobileParty();
                        all_recruits_party.StringId = "";
                        all_recruits_party.SetCustomName(new TextObject("{=CpuzeJFb}Available Troop Trees"));
                        //PartyScreenManager.OpenScreenAsLoot(Hero.OneToOneConversationHero.PartyBelongedTo.Party);
                        IEnumerable<CharacterObject> soldierlist = CharacterObject.FindAll(i => i.IsSoldier || i.IsRegular);
                        HashSet<CharacterObject> recruits = new HashSet<CharacterObject>(soldierlist);
                        foreach (CharacterObject soldier in soldierlist)
                        {
                            if (soldier.UpgradeTargets != null)
                                foreach (CharacterObject upgrade in soldier.UpgradeTargets)
                                {
                                    if (((soldier.IsSoldier && upgrade.IsSoldier) && (soldier.IsRegular && upgrade.IsRegular)) ||
                                        ((!soldier.IsSoldier && !upgrade.IsSoldier) && (soldier.IsRegular && upgrade.IsRegular)))
                                        recruits.Remove(upgrade);
                                }
                            else
                                recruits.Remove(soldier);
                        }
                        List<CharacterObject> sorted_recruits = recruits.ToList();
                        sorted_recruits.Sort((x, y) => x.Name.ToString().CompareTo(y.Name.ToString()));
                        foreach (CharacterObject soldier in sorted_recruits)
                        {
                            all_recruits_party.MemberRoster.AddToCounts(soldier, 1);
                        }
                        //all_recruits_party.IsVillager = true; // Why did I add this? Disabled since it's now read-only
                    }
                    var PSM_instance = Traverse.Create(PartyScreenManager.Instance);
                    PartyScreenLogic logic = new PartyScreenLogic();
                    PSM_instance.Field("_partyScreenLogic").SetValue(logic);
                    PSM_instance.Field("_currentMode").SetValue(PartyScreenMode.TroopsManage);
                    
                    logic.Initialize(template_party.Party, all_recruits_party, false, new TextObject("{=3AQlcqvU}Template"), 9999, new PartyPresentationDoneButtonDelegate(SelectTroopTreesDoneHandler), new TextObject("{=UoLVHbJh}Party Template Manager"));
                    logic.InitializeTrade(PartyScreenLogic.TransferState.Transferable, PartyScreenLogic.TransferState.NotTransferable, PartyScreenLogic.TransferState.NotTransferable);
                    logic.SetTroopTransferableDelegate(new PartyScreenLogic.IsTroopTransferableDelegate(PartyScreenManager.TroopTransferableDelegate));
                    logic.SetCancelActivateHandler(new PartyPresentationCancelButtonActivateDelegate(CancelHandler));
                    logic.SetDoneConditionHandler(new PartyPresentationDoneButtonConditionDelegate(PartyPresentationDoneButtonConditionDelegate));
                    PartyState partyState = Game.Current.GameStateManager.CreateState<PartyState>();
                    partyState.InitializeLogic(logic);
                    Game.Current.GameStateManager.PushState(partyState, 0);
                    InformationManager.DisplayMessage(new InformationMessage("Transfer the recruits of the troop tree(s) you want to manage in the next step.", Colors.Green));
                }
                catch (Exception e)
                {
                    MessageBox.Show(Utils.FlattenException(e));
                }
            }

            static void AddUpgrades(CharacterObject troop)
            {
                if (troop.UpgradeTargets != null)
                    foreach (CharacterObject upgrade in troop.UpgradeTargets)
                    {
                        if (upgrade != null)
                            if (!template_party.MemberRoster.Contains(upgrade))
                            {
                                //InformationManager.DisplayMessage(new InformationMessage(troop.Name.ToString(), Colors.Red));
                                template_party.MemberRoster.AddToCounts(upgrade, 1);
                                AddUpgrades(upgrade);
                            }
                    }
            }

            private void template_set_troop_limits()
            {
                try
                {
                    //TransferTroopPatch.ignore = true;
                    foreach (TroopRosterElement element in template_party.MemberRoster.GetTroopRoster())
                    {
                        AddUpgrades(element.Character);
                    }
                    if (Hero.OneToOneConversationHero.getTemplate() != null) {
                        TroopRoster old_template = Hero.OneToOneConversationHero.getTemplate();
                        foreach (TroopRosterElement element in old_template.GetTroopRoster())
                        {
                            if (template_party.MemberRoster.GetTroopCount(element.Character) == 0)
                                template_party.MemberRoster.AddToCounts(element.Character, element.Number);
                        }
                    }
                    //template_party.MemberRoster.Add(Hero.OneToOneConversationHero.getTemplate());
                    foreach (TroopRosterElement element in Hero.OneToOneConversationHero.PartyBelongedTo.MemberRoster.GetTroopRoster())
                    {
                        if (template_party.MemberRoster.GetTroopCount(element.Character) == 0 && !element.Character.IsHero)
                            template_party.MemberRoster.AddToCounts(element.Character, 1);
                    }
                    template_party.SetCustomName(new TextObject("{=9lssoqlP}Allowed Troops"));
                    if (template_limits_party == null)
                    {
                        template_limits_party = new MobileParty();
                        template_limits_party.StringId = "";
                        template_limits_party.SetCustomName(new TextObject("{=5R2m2nND}Add these to set Limits"));
                        foreach (TroopRosterElement element in template_party.MemberRoster.GetTroopRoster())
                        {
                            template_limits_party.AddElementToMemberRoster(element.Character, 1000);
                        }
                    }
                    var PSM_instance = Traverse.Create(PartyScreenManager.Instance);
                    PartyScreenLogic logic = new PartyScreenLogic();
                    PSM_instance.Field("_partyScreenLogic").SetValue(logic);
                    PSM_instance.Field("_currentMode").SetValue(PartyScreenMode.TroopsManage);

                    logic.Initialize(template_party.Party, template_limits_party, false, new TextObject("{=3AQlcqvU}Template"), 9999, new PartyPresentationDoneButtonDelegate(SelectTroopTreesDoneHandler), new TextObject("{=UoLVHbJh}Party Template Manager"));
                    logic.InitializeTrade(PartyScreenLogic.TransferState.Transferable, PartyScreenLogic.TransferState.NotTransferable, PartyScreenLogic.TransferState.NotTransferable);
                    logic.SetTroopTransferableDelegate(new PartyScreenLogic.IsTroopTransferableDelegate(PartyScreenManager.TroopTransferableDelegate));
                    logic.SetDoneConditionHandler(new PartyPresentationDoneButtonConditionDelegate(PartyPresentationDoneButtonConditionDelegate));
                    logic.SetCancelActivateHandler(new PartyPresentationCancelButtonActivateDelegate(CancelHandler));
                    PartyState partyState = Game.Current.GameStateManager.CreateState<PartyState>();
                    partyState.InitializeLogic(logic);
                    Game.Current.GameStateManager.PushState(partyState, 0);
                    //ScreenBase topScreen = ScreenManager.TopScreen;
                    //if (topScreen is GauntletPartyScreen)
                    //{
                    //    GauntletPartyScreen gps = topScreen as GauntletPartyScreen;
                    //    PartyVM partyVM = Traverse.Create(gps).Field<PartyVM>("_dataSource").Value;
                    //    Traverse.Create(partyVM).Field<PartyScreenLogic>("_partyScreenLogic").Value = logic;
                    //}
                    InformationManager.DisplayMessage(new InformationMessage("Troops with 1 member only will be recruited without limitation.\nAdd more from the right to set limits.", Colors.Green));
                }
                catch (Exception e)
                {
                    MessageBox.Show(Utils.FlattenException(e));
                }
            }

            public static Tuple<bool, TextObject> PartyPresentationDoneButtonConditionDelegate(TroopRoster leftMemberRoster, TroopRoster leftPrisonRoster, TroopRoster rightMemberRoster, TroopRoster rightPrisonRoster, int leftLimitNum, int rightLimitNum)
            {
                return new Tuple<bool, TextObject>(true, new TextObject("What?"));
            }

            private static bool CancelHandler()
            {
                return false;
            }

            private static bool SelectTroopTreesDoneHandler(TroopRoster leftMemberRoster, TroopRoster leftPrisonRoster, TroopRoster rightMemberRoster, TroopRoster rightPrisonRoster, FlattenedTroopRoster takenPrisonerRoster, FlattenedTroopRoster releasedPrisonerRoster, bool isForced, List<MobileParty> leftParties = null, List<MobileParty> rigthParties = null)
            {
                //IsMainTroopsLimitWarningEnabledPatch.ignore = true;
                return true;
            }

            private bool conversation_is_clan_member_not_in_party_on_condition()
            {
                return Hero.OneToOneConversationHero != null && Hero.OneToOneConversationHero.Clan == Hero.MainHero.Clan && Hero.MainHero.PartyBelongedTo != Hero.OneToOneConversationHero.PartyBelongedTo;
            }
            private bool conversation_is_clan_party_on_condition()
            {
                return Hero.OneToOneConversationHero != null && Hero.OneToOneConversationHero.PartyBelongedTo != null && Hero.OneToOneConversationHero.Clan == Hero.MainHero.Clan && Hero.MainHero.PartyBelongedTo != Hero.OneToOneConversationHero.PartyBelongedTo && !Hero.OneToOneConversationHero.PartyBelongedTo.IsCaravan;
            }

            private bool conversation_is_clan_party_or_caravan_on_condition()
            {
                return Hero.OneToOneConversationHero != null && Hero.OneToOneConversationHero.PartyBelongedTo != null && Hero.OneToOneConversationHero.Clan == Hero.MainHero.Clan && Hero.MainHero.PartyBelongedTo != Hero.OneToOneConversationHero.PartyBelongedTo;
            }

            private bool conversation_equipment_clan_reply_on_condition()
            {
                return Hero.OneToOneConversationHero.CharacterObject.Equipment.IsCivilian ? (civilian_equipment_backup.IsEquipmentEqualTo(Hero.OneToOneConversationHero.CivilianEquipment)) : (battle_equipment_backup.IsEquipmentEqualTo(Hero.OneToOneConversationHero.BattleEquipment));
            }

            private bool conversation_equipment_clan_reply_change_on_condition()
            {
                return Hero.OneToOneConversationHero.CharacterObject.Equipment.IsCivilian ? !(civilian_equipment_backup.IsEquipmentEqualTo(Hero.OneToOneConversationHero.CivilianEquipment)) : !(battle_equipment_backup.IsEquipmentEqualTo(Hero.OneToOneConversationHero.BattleEquipment));
            }

            private bool conversation_trade_party_clan_on_condition()
            {
                try
                {
                    OnTradeProfitMadePatch.enableProfitXP = false;
                    InventoryManager.OpenScreenAsInventoryOf(Hero.MainHero.PartyBelongedTo.Party, Hero.OneToOneConversationHero.PartyBelongedTo.Party);
                }
                catch (Exception e)
                {
                    MessageBox.Show(Utils.FlattenException(e));
                }
                return true;
            }

            private void conversation_equipment_party_clan_on_consequence()
            {
                try
                {
                    PartyBase rightparty = Hero.MainHero.PartyBelongedTo.Party;
                    PartyBase leftparty = Hero.OneToOneConversationHero.PartyBelongedTo.Party;
                    
                    
                    civilian_equipment_backup = Hero.OneToOneConversationHero.CivilianEquipment.Clone();
                    battle_equipment_backup = Hero.OneToOneConversationHero.BattleEquipment.Clone();

                    IMarketData marketdata = Traverse.Create<InventoryManager>().Method("GetCurrentMarketData").GetValue<IMarketData>();
                    InventoryLogic inventoryLogic;
                    if (leftparty != null)
                    {
                        //inventoryLogic = Traverse.Create(InventoryManager.Instance).Method("CreateSinglePlayerInventoryLogic", new Type[] { typeof(PartyBase) }).GetValue<InventoryLogic>(leftparty);
                        inventoryLogic = new InventoryLogic(Campaign.Current, leftparty);
                        inventoryLogic.Initialize(leftparty.ItemRoster, rightparty.ItemRoster, rightparty.MemberRoster, false, true, Hero.OneToOneConversationHero.CharacterObject, InventoryManager.InventoryCategoryType.None, marketdata, true, leftparty.Name);
                    }
                    else
                    {
                        // Does not work, can't pass null as a parameter even though the original game code sometimes does. Disabled equipment changing for governors. Change to empty inventory with "Discard" name?
                        inventoryLogic = new InventoryLogic(Campaign.Current, null);
                        inventoryLogic.Initialize(null, rightparty.ItemRoster, rightparty.MemberRoster, false, true, Hero.OneToOneConversationHero.CharacterObject, InventoryManager.InventoryCategoryType.None, marketdata, false, Hero.OneToOneConversationHero.Name);
                    }
                    inventoryLogic.AfterReset += ResetHeroEquipment;
                    InventoryState inventoryState = Game.Current.GameStateManager.CreateState<InventoryState>();
                    inventoryState.InitializeLogic(inventoryLogic);
                    Game.Current.GameStateManager.PushState(inventoryState, 0);
                    Traverse.Create(Campaign.Current.InventoryManager).Field<InventoryLogic>("_inventoryLogic").Value = inventoryLogic;
                    //PlayerEncounter.LeaveEncounter = true;
                }
                catch (Exception e)
                {
                    MessageBox.Show(Utils.FlattenException(e));
                }
            }
            private Equipment civilian_equipment_backup;
            private Equipment battle_equipment_backup;
            private void ResetHeroEquipment(InventoryLogic inventoryLogic)
            {
                if (battle_equipment_backup != null)
                    Hero.OneToOneConversationHero.BattleEquipment.FillFrom(battle_equipment_backup);
                if (civilian_equipment_backup != null)
                    Hero.OneToOneConversationHero.CivilianEquipment.FillFrom(civilian_equipment_backup);
                civilian_equipment_backup = null;
                battle_equipment_backup = null;
            }

            private bool conversation_troops_and_prisoners_party_clan_on_condition()
            {
                try
                {
                    MBTextManager.SetTextVariable("PARTY_LIST_TAG", Hero.OneToOneConversationHero.PartyBelongedTo.Party.Name, false);
                    PartyScreenManager.OpenScreenAsManageTroopsAndPrisoners(Hero.OneToOneConversationHero.PartyBelongedTo);
                }
                catch (Exception e)
                {
                    MessageBox.Show(Utils.FlattenException(e));
                }
                return true;
            }

            private bool get_is_line_on_page(bool othercondition)
            {
                if (othercondition)
                {
                    if ((line_index < lines_current_page * lines_per_page) && (line_index >= (lines_current_page - 1) * lines_per_page))
                    {
                        line_index++;
                        return true;
                    }
                    line_index++;
                }
                return false;
            }

            private bool give_party_order_patrol_additional_condition()
            {
                for (int i = 1; i <= 3; i++)
                    MBTextManager.SetTextVariable("ADDITIONAL_ORDERS_PATROL_" + i.ToString(), "", false);
                if (order.FriendlyVillagesScoreMultiplier == 1f && order.PartyMaintenanceScoreMultiplier == 1f && order.OwnClanVillagesScoreMultiplier == 1f)
                    MBTextManager.SetTextVariable("ADDITIONAL_ORDERS_PATROL_1", "{=DpjCas89}Any additional instructions to follow during the patrol?", false);
                else if (order.FriendlyVillagesScoreMultiplier < 1f)
                    MBTextManager.SetTextVariable("ADDITIONAL_ORDERS_PATROL_1", "{=gZ3JPgdJ}The villages of our allied clans will have to do without us.\n", false);
                if (order.OwnClanVillagesScoreMultiplier > 1f)
                    MBTextManager.SetTextVariable("ADDITIONAL_ORDERS_PATROL_2", "{=xVmFj0Vt}We'll come to the aid of more distant villages as well.\n", false);
                if (order.PartyMaintenanceScoreMultiplier > 1f)
                    MBTextManager.SetTextVariable("ADDITIONAL_ORDERS_PATROL_3", "{=FZsSqjwF}We shall frequently visit nearby settlements for troops and to trade loot, supplies and prisoners.", false);
                return true;
            }

            private bool give_party_order_escort_additional_condition()
            {
                string _0 = "";
                string _1 = "{=2EyKGT16}Any additional instructions to follow while escorting you?";
                string _2 = "{=hg0GrxBV}We won't flee from enemies stronger than us.";
                string _3 = "{=BEdedw1C}We won't chase any enemies unless you give the signal.";
                string _4 = "{=DL3kEH8n}We shall stay at your side until commanded otherwise.";
                string _5 = "\nWe'll only take enemy leaders as prisoner.";
                if (order.AttackInitiative == 1f && order.AvoidInitiative == 1f)
                    _0 = _1;
                if (order.AvoidInitiative == 0f)
                    _0 = _2;
                if (order.AttackInitiative == 0f)
                    _0 = _3;
                if (order.AttackInitiative == 0f && order.AvoidInitiative == 0f)
                    _0 = _4;
                if (order.StopTakingPrisoners)
                    _0 += _5;
                MBTextManager.SetTextVariable("ADDITIONAL_ORDERS_ESCORT", _0, false);
                return true;
            }

            private bool give_party_order_roam_additional_condition()
            {
                for (int i = 1; i <= 8; i++)
                    MBTextManager.SetTextVariable("ADDITIONAL_ORDERS_ROAM_" + i.ToString(), "", false);
                if (order.FriendlyVillagesScoreMultiplier == 1f && !findplayerifbored && order.LeaveTroopsToGarrisonOtherClans && order.AllowJoiningArmies && order.AllowRaidingVillages && order.HostileSettlementsScoreMultiplier > 0f && order.AttackInitiative == 1f && order.AvoidInitiative == 1f)
                    MBTextManager.SetTextVariable("ADDITIONAL_ORDERS_ROAM_1", "{=EBO8npRn}Any additional instructions to follow while roaming the lands?", false);
                else if (order.FriendlyVillagesScoreMultiplier < 1f)
                    MBTextManager.SetTextVariable("ADDITIONAL_ORDERS_ROAM_1", "{=n8KqPSs3}I won't help defending settlements of allied clans.\n", false);
                if (!order.LeaveTroopsToGarrisonOtherClans)
                    MBTextManager.SetTextVariable("ADDITIONAL_ORDERS_ROAM_2", "{=hg8h2ZRQ}I'll never give my troops to garrisons of allied clans.\n", false);
                if (!order.AllowJoiningArmies)
                    MBTextManager.SetTextVariable("ADDITIONAL_ORDERS_ROAM_3", "{=GKLbkTYg}I won't join armies unless you call for me.\n", false);
                if (!order.AllowRaidingVillages)
                    MBTextManager.SetTextVariable("ADDITIONAL_ORDERS_ROAM_4", "{=Et1Fdug7}I won't raid any villages.\n", false);
                if (order.HostileSettlementsScoreMultiplier == 0f)
                    MBTextManager.SetTextVariable("ADDITIONAL_ORDERS_ROAM_5", "{=xCAkZSk8}I'll ignore opportunities to conquer territory.\n", false);
                if (order.AvoidInitiative > 1f)
                    MBTextManager.SetTextVariable("ADDITIONAL_ORDERS_ROAM_6", "{=s3xzYOlT}We'll run from anyone who may cause us significant losses.\n", false);
                if (order.AttackInitiative < 1f)
                    MBTextManager.SetTextVariable("ADDITIONAL_ORDERS_ROAM_7", "{=2wfMJTSO}We'll chase only the most tempting enemy parties.\n", false);
                if (findplayerifbored)
                    MBTextManager.SetTextVariable("ADDITIONAL_ORDERS_ROAM_8", "{=66JVQdWZ}We'll come follow you if I can't think of much else to do.", false);
                return true;
            }

            private void give_party_order_roam_confirm_consequence()
            {
                order.OwnerHero.cancelOrder();
                order.OwnerHero.PartyBelongedTo.Army = null;
                if (findplayerifbored) 
                {
                    order.Behavior = AiBehavior.EscortParty;
                    order.TargetParty = Hero.MainHero.PartyBelongedTo;
                    order.ScoreMinimum = 0.25f;
                    order.ScoreMultiplier = 1f;
                    order.OwnerHero.PartyBelongedTo.SetMoveEscortParty(order.TargetParty);
                } else
                {
                    order.Behavior = AiBehavior.None;
                    order.ScoreMinimum = 0f;
                    order.ScoreMultiplier = 0f;
                    order.OwnerHero.PartyBelongedTo.SetMoveModeHold();
                }
                PartyAICommandsBehavior.RegisterOrder(order.OwnerHero, order);
                //party.SetInititave(attackInitiative, avoidInitiative, CampaignTime.YearsFromNow(100).RemainingHoursFromNow);
                
            }

            private void give_party_order_patrol_confirm_consequence()
            {
                //order.FriendlyVillagesScoreMultiplier;
                //order.OwnClanVillagesScoreMultiplier;
                //order.PartyMaintenanceScoreMultiplier;
                order.OwnerHero.cancelOrder();
                order.OwnerHero.PartyBelongedTo.Army = null;

                order.Behavior = AiBehavior.PatrolAroundPoint;
                order.ScoreMinimum = 0.5f;
                order.ScoreMultiplier = 1.3f;
                order.HostileSettlementsScoreMultiplier = 0.1f;
                order.LeaveTroopsToGarrisonOtherClans = false;
                order.AllowJoiningArmies = false;
                order.AllowRaidingVillages = false;

                PartyAICommandsBehavior.RegisterOrder(order.OwnerHero, order);
                order.OwnerHero.PartyBelongedTo.SetMovePatrolAroundSettlement(order.TargetSettlement);
            }

            private void give_party_order_escort_confirm_consequence()
            {
                order.OwnerHero.cancelOrder();
                order.OwnerHero.PartyBelongedTo.Army = null;

                order.Behavior = AiBehavior.EscortParty;
                order.TargetParty = Hero.MainHero.PartyBelongedTo;
                order.ScoreMinimum = 15f;
                order.ScoreMultiplier = 1f;
                order.FriendlyVillagesScoreMultiplier = 0.5f;
                order.OwnClanVillagesScoreMultiplier = 0.5f;
                order.PartyMaintenanceScoreMultiplier = 0.5f;
                order.HostileSettlementsScoreMultiplier = 0.1f;
                order.LeaveTroopsToGarrisonOtherClans = false;
                order.AllowJoiningArmies = false;
                order.AllowRaidingVillages = false;

                PartyAICommandsBehavior.RegisterOrder(order.OwnerHero, order);
                order.OwnerHero.PartyBelongedTo.SetMoveEscortParty(order.TargetParty);
                if (Hero.OneToOneConversationHero.PartyBelongedTo.GetNumDaysForFoodToLast() < 3)
                {
                    InformationManager.DisplayMessage(new InformationMessage(Hero.OneToOneConversationHero.PartyBelongedTo.Name + " is short on food.", Colors.Red));
                }
            }

            private void conversation_lord_leave_on_consequence()
            {
                if (PlayerEncounter.Current != null)
                {
                    PlayerEncounter.LeaveEncounter = true;
                }
            }

            private void MergeDisbandParty(MobileParty disbandParty, PartyBase mergeToParty)
            {
                disbandParty.LeaderHero.cancelOrder();
                mergeToParty.ItemRoster.Add(disbandParty.ItemRoster.AsEnumerable());
                //FlattenedTroopRoster flattenedTroopRoster = new FlattenedTroopRoster();
                foreach (TroopRosterElement troopRosterElement in disbandParty.PrisonRoster.GetTroopRoster())
                {
                    if (troopRosterElement.Character.IsHero)
                    {
                        GivePrisonerAction.Apply(troopRosterElement.Character, disbandParty.Party, mergeToParty);
                    }
                    else
                    {
                        //flattenedTroopRoster.Add(troopRosterElement.Character, troopRosterElement.Number, troopRosterElement.WoundedNumber);
                        mergeToParty.PrisonRoster.AddToCounts(troopRosterElement.Character, troopRosterElement.Number, false, troopRosterElement.WoundedNumber, troopRosterElement.Xp, true, -1);
                    }
                }
                foreach (TroopRosterElement troopRosterElement2 in disbandParty.MemberRoster.GetTroopRoster().ToList<TroopRosterElement>())
                {
                    disbandParty.MemberRoster.RemoveTroop(troopRosterElement2.Character, 1, default(UniqueTroopDescriptor), 0);
                    if (troopRosterElement2.Character.IsHero)
                    {
                        AddHeroToPartyAction.Apply(troopRosterElement2.Character.HeroObject, mergeToParty.MobileParty, true);
                    }
                    else
                    {
                        mergeToParty.MemberRoster.AddToCounts(troopRosterElement2.Character, troopRosterElement2.Number, false, troopRosterElement2.WoundedNumber, troopRosterElement2.Xp, true, -1);
                    }
                }
                //mergeToParty.AddPrisoners(flattenedTroopRoster);
                disbandParty.RemoveParty();
            }

            private void join_army()
            {
                PlayerEncounter.LeaveEncounter = true;
                if (MobileParty.MainParty.Army == null)
                {
                    if (Clan.PlayerClan.IsUnderMercenaryService || Clan.PlayerClan.Kingdom == null)
                        CreateArmy(Hero.MainHero, Hero.MainHero.HomeSettlement, Army.ArmyTypes.Patrolling);
                    else
                        Clan.PlayerClan.Kingdom.CreateArmy(Hero.MainHero, Hero.MainHero.HomeSettlement, Army.ArmyTypes.Patrolling);
                }
                Hero.OneToOneConversationHero.cancelOrder();
                Hero.OneToOneConversationHero.PartyBelongedTo.Army = MobileParty.MainParty.Army;
                SetPartyAiAction.GetActionForEscortingParty(Hero.OneToOneConversationHero.PartyBelongedTo, MobileParty.MainParty);
                Hero.OneToOneConversationHero.PartyBelongedTo.IsJoiningArmy = true;
            }

            private void CreateArmy(Hero armyLeader, IMapPoint target, Army.ArmyTypes selectedArmyType)
            {
                if (!armyLeader.IsActive)
                {
                    return;
                }
                if (((armyLeader != null) ? armyLeader.PartyBelongedTo.Leader : null) != null)
                {
                    Army army = new Army(null, armyLeader.PartyBelongedTo, selectedArmyType, target, null)
                    {
                        AIBehavior = Army.AIBehaviorFlags.Gathering
                    };
                    army.Gather();
                    var method = Traverse.Create(CampaignEventDispatcher.Instance).Method("OnArmyCreated", new Type[] { typeof(Army) });
                    if (method.MethodExists())
                        method.GetValue(army);
                    else
                        MessageBox.Show("Party AI Overhaul and Commands: Cannot find dispatch method OnArmyCreated, needs update.");
                    //CampaignEventDispatcher.Instance.OnArmyCreated(army);
                }
                if (armyLeader == Hero.MainHero)
                {
                    MapState mapState = Game.Current.GameStateManager.GameStates.Single((GameState S) => S is MapState) as MapState;
                    if (mapState == null)
                    {
                        return;
                    }
                    mapState.OnArmyCreated(MobileParty.MainParty);
                }
            }
        }
    }
}
