﻿using HarmonyLib;
using PartyAIOverhaulCommands.src.Behaviours;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaleWorlds.CampaignSystem;
using TaleWorlds.Library;
using TaleWorlds.SaveSystem;

namespace PartyAIOverhaulCommands
{
    public partial class PartyOrder
    {
		public partial class PartyOrderBuilder { }

		public PartyOrder(Hero owner)
		{
			this._ownerHero = owner;
			this._attackInitiative = 1f;
			this._avoidInitiative = 1f;
		}

		public float getScore(float base_score = 0f)
		{
			return (Math.Max(base_score * this.ScoreMultiplier, this.ScoreMinimum));
		}

		[SaveableProperty(1)]
		public MobileParty TargetParty { get; private set; }

		[SaveableProperty(2)]
		public Settlement TargetSettlement { get; private set; }

		[SaveableProperty(3)]
		public AiBehavior Behavior { get; private set; }

		[SaveableProperty(4)]
		public float ScoreMultiplier { get; private set; } = 1f;

		[SaveableProperty(5)]
		public float ScoreMinimum { get; private set; } = 0f;

		[SaveableProperty(6)]
		public float HostileSettlementsScoreMultiplier { get; private set; } = 1f;

		[SaveableProperty(7)]
		public float FriendlyVillagesScoreMultiplier { get; private set; } = 1f;

		[SaveableProperty(8)]
		public float PartyMaintenanceScoreMultiplier { get; private set; } = 1f;

		[SaveableProperty(9)]
		public float OwnClanVillagesScoreMultiplier { get; private set; } = 1f;

		[SaveableProperty(10)]
		public bool LeaveTroopsToGarrisonOtherClans { get; private set; } = true;

		[SaveableProperty(11)]
		public bool AllowRaidingVillages { get; private set; } = true;

		[SaveableProperty(12)]
		public bool AllowJoiningArmies { get; private set; } = true;

		[SaveableField(13)]
		private MobileParty _tempTargetParty;
		public MobileParty TempTargetParty { get
			{
				return _tempTargetParty;
			}
			set
			{
				try
				{
					if (value == null)
					{
						_tempTargetParty = null;
						if (OwnerParty != null)
							OwnerParty.Ai.SetDoNotMakeNewDecisions(false);
					}
					else
					{
						_tempTargetParty = value;
						if (OwnerParty != null)
							OwnerParty.Ai.SetDoNotMakeNewDecisions(true);
					}
				}
				catch (Exception e)
				{
					MessageBox.Show(Utils.FlattenException(e));
				}
			}
		}

		[SaveableField(14)]
		private MobileParty _ownerParty; //Deprecated since 1.4.3, leave anyway to prevent potential savegame corruption issues?
		public MobileParty OwnerParty { 
			get 
			{
				return OwnerHero.PartyBelongedTo;
			}
		}

		[SaveableField(15)]
		private Hero _ownerHero;
		public Hero OwnerHero
		{
			get
			{
				if (_ownerHero == null)
				{
					_ownerHero = PartyAICommandsBehavior.Instance.order_map.FirstOrDefault(x => x.Value == this).Key;
				}
				return _ownerHero;
			}
		}

		[SaveableField(16)]
		private float _avoidInitiative = -1f;
		public float AvoidInitiative
		{
			get
			{
				if (_avoidInitiative < 0f && this.OwnerParty != null)
					_avoidInitiative = Traverse.Create(this.OwnerParty).Field("_avoidInitiative").GetValue<float>();
				return _avoidInitiative;
			}
			set
			{
				_avoidInitiative = value;
				if (this.OwnerParty != null)
					this.OwnerParty.SetInititave(AttackInitiative, AvoidInitiative, CampaignTime.YearsFromNow(100).RemainingHoursFromNow);
			}
		}

		[SaveableField(17)]
		private float _attackInitiative = -1f;
		public float AttackInitiative
		{
			get
			{
				if (_attackInitiative < 0f && this.OwnerParty != null)
					_attackInitiative = Traverse.Create(this.OwnerParty).Field("_attackInitiative").GetValue<float>();
				return _attackInitiative;
			}
			set
			{
				_attackInitiative = value;
				if (this.OwnerParty != null)
					this.OwnerParty.SetInititave(AttackInitiative, AvoidInitiative, CampaignTime.YearsFromNow(100).RemainingHoursFromNow);
			}
		}

		[SaveableProperty(18)]
		public bool StopRecruitingTroops { get; private set; }

		[SaveableProperty(19)]
		public bool StopTakingPrisoners { get; private set; }
	}

	public static class HeroExtension
	{
		public static void cancelOrder(this Hero leader)
		{
			try
			{
				if (leader?.getOrder() != null)
				{
					MobileParty party = leader.PartyBelongedTo;
					PartyAICommandsBehavior.Instance.order_map.Remove(leader);
					if (party != null)
					{
						party.SetInititave(1f, 1f, 0f);
						party.Ai.SetDoNotMakeNewDecisions(false);
					}
				}
			}
			catch (Exception e)
			{
				MessageBox.Show(Utils.FlattenException(e));
			}
		}

		public static PartyOrder getOrder(this Hero leader)
		{
			try
			{
				if (PartyAICommandsBehavior.Instance.order_map == null)
					return null;
				PartyOrder order;
				PartyAICommandsBehavior.Instance.order_map.TryGetValue(leader, out order);
				return order;
			}
			catch (Exception e)
			{
				MessageBox.Show(Utils.FlattenException(e));
				return null;
			}
		}

		public static TroopRoster getTemplate(this Hero leader)
		{
			try
			{
				if (PartyAICommandsBehavior.Instance.template_map == null)
					return null;
				TroopRoster template;
				PartyAICommandsBehavior.Instance.template_map.TryGetValue(leader, out template);
				return template;
			}
			catch (Exception e)
			{
				MessageBox.Show(Utils.FlattenException(e));
				return null;
			}
		}
	}
}
