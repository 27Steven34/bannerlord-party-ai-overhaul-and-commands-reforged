﻿using HarmonyLib;
using System;
using System.Reflection;
using System.Windows.Forms;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.SandBox.GameComponents;
using TaleWorlds.CampaignSystem.SandBox.GameComponents.Map;
using TaleWorlds.Core;

namespace PartyAIOverhaulCommands
{
    [HarmonyPatch]
    public class FindNumberOfTroopsToLeaveToGarrisonPatch
    {
        static MethodBase TargetMethod()
        {
            Type mytype = AccessTools.TypeByName("DefaultSettlementGarrisonModel");
            return AccessTools.Method(mytype, "FindNumberOfTroopsToLeaveToGarrison",
                new Type[] { typeof(MobileParty), typeof(Settlement) }
            );
        }
        static void Postfix(MobileParty mobileParty, Settlement settlement, ref int __result)
        {
            if ((mobileParty?.LeaderHero?.getOrder() != null && settlement.OwnerClan != mobileParty.LeaderHero.Clan && !mobileParty.LeaderHero.getOrder().LeaveTroopsToGarrisonOtherClans) ||
                (mobileParty.Army != null && mobileParty.LeaderHero.Clan == Hero.MainHero.Clan && mobileParty.Army.LeaderParty == Hero.MainHero.PartyBelongedTo))
            {
                __result = 0;
            }
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }

        static bool Prepare()
        {
            return true;
        }
    }
}
