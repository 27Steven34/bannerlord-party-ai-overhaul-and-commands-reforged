﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.SandBox.CampaignBehaviors.AiBehaviors;
using TaleWorlds.CampaignSystem.SandBox.GameComponents.Map;

namespace PartyAIOverhaulCommands
{
    // Stops parties from joining armies if ordered not to
    [HarmonyPatch(typeof(AiArmyMemberBehavior), "AiHourlyTick")]
    [HarmonyPatch(new Type[] { typeof(MobileParty), typeof(PartyThinkParams) })]
    public class AiArmyMemberBehaviorPatch
    {
        static bool Prefix(MobileParty mobileParty, PartyThinkParams p)
        {
            if (mobileParty?.LeaderHero?.getOrder() == null || mobileParty.Army == null || mobileParty.Army.LeaderParty == mobileParty || mobileParty.IsDeserterParty)
            {
                return true;
            }
            if (!mobileParty.LeaderHero.getOrder().AllowJoiningArmies && !(mobileParty.Army.LeaderParty == Hero.MainHero.PartyBelongedTo))
            {
                //MessageBox.Show("Party AI Overhaul and Commands: Please notify the developer that the following patch runs after all: AiArmyMemberBehaviorPatch.\nThis message will then be removed in the next update. This is not a bug, you can continue playing now.");
                mobileParty.Army = null;
                return false;
            }
            return true;
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }

        static bool Prepare()
        {
            return true;
        }
    }

    [HarmonyPatch(typeof(DefaultArmyManagementCalculationModel), "GetMobilePartiesToCallToArmy")]
    public class GetMobilePartiesToCallToArmyPatch
    {
        static void Postfix(ref List<MobileParty> __result, MobileParty leaderParty)
        {
            if (leaderParty != MobileParty.MainParty && leaderParty.MapFaction == MobileParty.MainParty.MapFaction)
            {
                for (int i = 0; i < __result.Count; i++)
                {
                    if ((__result[i]?.LeaderHero?.getOrder()?.AllowJoiningArmies == false && __result[i]?.LeaderHero != leaderParty?.LeaderHero) || __result[i] == MobileParty.MainParty)
                    {
                        //MessageBox.Show("Prevented the following party from joining " + leaderParty.Name + " army: " + __result[i].Name + " Index number: " + i.ToString() + " Next in list: " + __result[i+1].Name);
                        __result.RemoveAt(i);
                        i--;
                    }
                }
            }
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }

        static bool Prepare()
        {
            return true;
        }
    }


}
