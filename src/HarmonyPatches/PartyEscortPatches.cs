﻿using HarmonyLib;
using PartyAIOverhaulCommands.src.Behaviours;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaleWorlds.CampaignSystem;
using TaleWorlds.Core;
using TaleWorlds.InputSystem;
using TaleWorlds.Library;

namespace PartyAIOverhaulCommands
{
    [HarmonyPatch(typeof(MobileParty), "SetMoveEngageParty")]
    [HarmonyPatch(new Type[] { typeof(MobileParty) })]
    public class SetMoveEngagePartyPatch
    {
        public static void Postfix(MobileParty __instance, MobileParty party)
        {
            if (party?.LeaderHero?.getOrder() != null && party.LeaderHero.getOrder().Behavior == AiBehavior.EscortParty && party.LeaderHero.getOrder().TargetParty == __instance)
            {
                // Stop escort party from moving when player clicks on escort
                //InformationManager.DisplayMessage(new InformationMessage(party.Name + " awaits your arrival."));
                party.SetMoveModeHold();
            }
            else if (Input.IsKeyDown((InputKey)Config.Value.OrderEscortEngageHoldKey))
            {
                foreach (KeyValuePair<Hero, PartyOrder> pair in PartyAICommandsBehavior.Instance.order_map)
                {
                    Hero leader = pair.Key;
                    PartyOrder order = pair.Value;
                    
                    if (leader != null && order != null && leader.PartyBelongedTo != null && order.Behavior == AiBehavior.EscortParty && order.TargetParty == __instance)
                    {
                        MobileParty ordered_party = leader.PartyBelongedTo;
                        float distance = Campaign.Current.Models.MapDistanceModel.GetDistance(__instance, ordered_party);
                        if (distance < ordered_party.SeeingRange)
                            if (!FactionManager.IsAtWarAgainstFaction(party.MapFaction, __instance.MapFaction))
                                // Target is not hostile, make sure escort keeps escorting
                                ordered_party.SetMoveEscortParty(__instance);
                            else
                            {
                                // Order our escort parties that are close by to engage our target
                                ordered_party.SetMoveEngageParty(party);
                                order.TempTargetParty = party;
                            }
                        else if (distance < __instance.SeeingRange)
                        {
                            InformationManager.DisplayMessage(new InformationMessage(ordered_party.Name + " can't see our signals!", Colors.Red));
                        }
                    }
                }
            }
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }

    [HarmonyPatch(typeof(MobileParty), "SetMoveEscortParty")]
    [HarmonyPatch(new Type[] { typeof(MobileParty) })]
    public class SetMoveEscortPartyPatch
    {
        static void Postfix(MobileParty __instance, MobileParty mobileParty)
        {
            SetMoveEngagePartyPatch.Postfix(__instance, mobileParty);
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }


    [HarmonyPatch(typeof(MobileParty), "SetMoveGoToSettlement")]
    [HarmonyPatch(new Type[] { typeof(Settlement) })]
    public class SetMoveGoToSettlementPatch
    {
        static void Postfix(MobileParty __instance, Settlement settlement)
        {
            if (Input.IsKeyDown((InputKey)Config.Value.OrderEscortEngageHoldKey) && __instance.IsMainParty)
            {
                foreach (KeyValuePair<Hero, PartyOrder> pair in PartyAICommandsBehavior.Instance.order_map)
                {
                    Hero leader = pair.Key;
                    PartyOrder order = pair.Value;
                    if (leader != null && order != null && leader.PartyBelongedTo != null && order.Behavior == AiBehavior.EscortParty && order.TargetParty == __instance)
                    {
                        MobileParty ordered_party = leader.PartyBelongedTo;
                        float distance = Campaign.Current.Models.MapDistanceModel.GetDistance(__instance, ordered_party);
                        if (distance < ordered_party.SeeingRange)
                        {
                            ordered_party.SetMoveEscortParty(__instance);
                            order.TempTargetParty = null;
                        }
                        else if (distance < __instance.SeeingRange)
                        {
                            InformationManager.DisplayMessage(new InformationMessage(ordered_party.Name + " can't see our signals!", Colors.Red));
                        }
                    }
                }
            }
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }


    [HarmonyPatch(typeof(MobileParty), "SetMoveGoToPoint")]
    [HarmonyPatch(new Type[] { typeof(Vec2) })]
    public class SetMoveGoToPointPatch
    {
        static void Postfix(MobileParty __instance, Vec2 point)
        {
            if (Input.IsKeyDown((InputKey)Config.Value.OrderEscortEngageHoldKey) && __instance.IsMainParty)
            {
                foreach (KeyValuePair<Hero, PartyOrder> pair in PartyAICommandsBehavior.Instance.order_map)
                {
                    Hero leader = pair.Key;
                    PartyOrder order = pair.Value;
                    if (leader != null && order != null && leader.PartyBelongedTo != null && order.Behavior == AiBehavior.EscortParty && order.TargetParty == __instance)
                    {
                        MobileParty ordered_party = leader.PartyBelongedTo;
                        float distance = Campaign.Current.Models.MapDistanceModel.GetDistance(__instance, ordered_party);
                        if (distance < ordered_party.SeeingRange)
                        {
                            ordered_party.SetMoveEscortParty(__instance);
                            order.TempTargetParty = null;
                        }
                        else if (distance < __instance.SeeingRange)
                        {
                            InformationManager.DisplayMessage(new InformationMessage(ordered_party.Name + " can't see our signals!", Colors.Red));
                        }
                    }
                }
            }
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }


    [HarmonyPatch(typeof(PlayerEncounter), "Finish")]
    [HarmonyPatch(new Type[] { typeof(bool) })]
    public class PlayerEncounterPatch
    {
        static bool Prefix(bool forcePlayerOutFromSettlement)
        {
            if (!forcePlayerOutFromSettlement && PlayerEncounter.EncounteredMobileParty?.LeaderHero?.getOrder() != null && PlayerEncounter.EncounteredMobileParty.LeaderHero.getOrder().Behavior == AiBehavior.EscortParty && PlayerEncounter.EncounteredMobileParty.LeaderHero.getOrder().TargetParty == Hero.MainHero.PartyBelongedTo)
            {
                // Makes sure escorts immediately resume escorting after talking to them
                PlayerEncounter.EncounteredMobileParty.SetMoveEscortParty(PlayerEncounter.EncounteredMobileParty.LeaderHero.getOrder().TargetParty);
                PlayerEncounter.EncounteredMobileParty.LeaderHero.getOrder().TempTargetParty = null;
                //InformationManager.DisplayMessage(new InformationMessage("RemoveInvolvedParty"));
            }
            return true;
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }

    [HarmonyPatch(typeof(MobileParty), "OnEventEnded")]
    [HarmonyPatch(new Type[] { typeof(MapEvent) })]
    public class OnEventEndedPatch
    {
        static void Postfix(MobileParty __instance, MapEvent mapEvent)
        {
            if (__instance?.LeaderHero?.getOrder() != null && __instance.LeaderHero.getOrder().Behavior == AiBehavior.EscortParty)
            {
                // After escort is exiting a battle, immediately resume escorting
                //__instance.SetMoveEscortParty(__instance.LeaderHero.getOrder().TargetParty);
                //__instance.LeaderHero.getOrder().TempTargetParty = null;
                //InformationManager.DisplayMessage(new InformationMessage("OnEventEnded - Follow Player"));

                foreach (KeyValuePair<Hero, PartyOrder> pair in PartyAICommandsBehavior.Instance.order_map)
                {
                    Hero leader = pair.Key;
                    PartyOrder order = pair.Value;
                    if (leader != null && order != null && leader.PartyBelongedTo != null && order.Behavior == AiBehavior.EscortParty && order.TargetParty == MobileParty.MainParty)
                    {
                        leader.PartyBelongedTo.SetMoveEscortParty(order.TargetParty);
                        order.TempTargetParty = null;
                    }
                }
            }
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }
}
