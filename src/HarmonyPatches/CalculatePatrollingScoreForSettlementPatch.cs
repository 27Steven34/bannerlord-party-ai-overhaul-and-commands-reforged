﻿using HarmonyLib;
using System;
using System.Reflection;
using System.Windows.Forms;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.SandBox.GameComponents;
using TaleWorlds.CampaignSystem.SandBox.GameComponents.Map;
using TaleWorlds.Core;

namespace PartyAIOverhaulCommands
{
    [HarmonyPatch(typeof(DefaultTargetScoreCalculatingModel), "CalculatePatrollingScoreForSettlement")]
    public class CalculatePatrollingScoreForSettlementPatch
    {
        static void Postfix(DefaultTargetScoreCalculatingModel __instance, Settlement settlement, MobileParty mobileParty, ref float __result)
        {
            if (mobileParty?.LeaderHero?.getOrder() != null)
            {
                PartyOrder order = mobileParty.LeaderHero.getOrder();

                if (settlement.OwnerClan == mobileParty.LeaderHero.Clan)
                {
                    __result *= order.OwnClanVillagesScoreMultiplier;
                } else
                {
                    __result *= order.FriendlyVillagesScoreMultiplier;
                }
            }
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }

        static bool Prepare()
        {
            return false;
        }
    }
}
