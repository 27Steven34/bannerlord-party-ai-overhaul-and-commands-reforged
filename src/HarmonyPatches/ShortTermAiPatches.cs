﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaleWorlds.CampaignSystem;
using TaleWorlds.Core;
using TaleWorlds.Library;

namespace PartyAIOverhaulCommands
{
    [HarmonyPatch(typeof(MobileParty), "GetBestInitiativeBehavior")]
    public class GetBestInitiativeBehaviorPatch
    {
        public static bool parties_around_position_patched = false;
        public static bool parties_distance_patched = false;
        // Raises short term AI party reaction range
        static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
        {
            MethodInfo getSeeingRange = AccessTools.Property(typeof(MobileParty), nameof(MobileParty.SeeingRange)).GetGetMethod();
            MethodInfo Distance = AccessTools.Method(typeof(Vec2), nameof(Vec2.Distance));
            MethodInfo inAiRange = AccessTools.Method(typeof(GetBestInitiativeBehaviorPatch), nameof(GetBestInitiativeBehaviorPatch.inAiRange));

            var codes = new List<CodeInstruction>(instructions);
            for (int i = 0; i < codes.Count && i < 220; i++)
            {
                // Looks for:   List<MobileParty> partiesAroundPosition = this._partiesAroundPosition.GetPartiesAroundPosition(this.Position2D, 9f);
                if (!parties_around_position_patched &&
                    codes[i].opcode == OpCodes.Ldc_R4 &&
                    codes[i + 1].opcode == OpCodes.Callvirt && (codes[i + 1].operand as MethodInfo)?.Name == "GetPartiesAroundPosition")
                {
                    codes[i] = new CodeInstruction(OpCodes.Ldarg_0);
                    codes.Insert(i + 1, new CodeInstruction(OpCodes.Call, getSeeingRange));
                    parties_around_position_patched = true;
                    // Changed to:   List<MobileParty> partiesAroundPosition = this._partiesAroundPosition.GetPartiesAroundPosition(this.Position2D, this.GetSeeingRange);
                }
                // Looks for:   if (num < 6f)
                if (!parties_distance_patched &&
                    codes[i].opcode == OpCodes.Bge &&
                    codes[i - 1].opcode == OpCodes.Ldc_R4 && codes[i - 1].operand as float? == 6f &&
                    codes[i - 4].opcode == OpCodes.Call && codes[i - 4].operand as MethodInfo == Distance)
                {
                    codes[i].opcode = OpCodes.Brfalse;
                    codes[i - 1] = new CodeInstruction(OpCodes.Call, inAiRange);
                    codes.Insert(i - 2, new CodeInstruction(OpCodes.Ldarg_0));
                    parties_distance_patched = true;
                    break;
                    // Changed to:   if (inAiRange(this, num))
                    // where num is the distance between two parties, and inAiRange is a new method
                }
            }
            return codes.AsEnumerable();
        }


        // Increases AI score to be more aggressive under particular conditions
        static void Postfix(MobileParty __instance, float ____attackInitiative, float ____avoidInitiative, AiBehavior bestInitiativeBehavior, MobileParty bestInitiativeTargetParty, ref float bestInitiativeBehaviorScore, Vec2 avarageEnemyVec)
        {
            if (__instance?.LeaderHero?.getOrder()?.Behavior != null && bestInitiativeTargetParty != null && !__instance.IsJoiningArmy)
            {
                if (bestInitiativeBehavior == AiBehavior.EngageParty && bestInitiativeTargetParty.IsBandit && (__instance.LeaderHero.getOrder().Behavior == AiBehavior.PatrolAroundPoint || (__instance.LeaderHero.getOrder().Behavior == AiBehavior.EscortParty && ____attackInitiative > 0f)))
                {
                    Vec2 relative_target_position = ((bestInitiativeTargetParty.BesiegedSettlement != null) ? bestInitiativeTargetParty.GetVisualPosition().AsVec2 : bestInitiativeTargetParty.Position2D) - __instance.Position2D;
                    if (bestInitiativeBehaviorScore < 1.1f && ((__instance.GetCachedPureSpeed() > bestInitiativeTargetParty.GetCachedPureSpeed() * 1.05) || bestInitiativeTargetParty.Bearing.DotProduct(relative_target_position) <= 0))
                    {
                        // Perhaps take into account distance from patrol position?
                        bestInitiativeBehaviorScore = 1.1f;
                    }
                    //InformationManager.DisplayMessage(new InformationMessage("Best: " + bestInitiativeTargetParty.Name + " " + bestInitiativeBehaviorScore.ToString()));
                }
            }
        }

        public static bool inAiRange(MobileParty party, float distance)
        {
            if (party?.LeaderHero?.getOrder() != null && distance < party.SeeingRange)
            {
                return true;
            }
            else if (distance < 6.0f)
            {
                return true;
            }
            return false;
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }


    [HarmonyPatch(typeof(MobileParty), "CalculateInitiativeScoresForEnemy")]
    public class CalculateInitiativeScoresForEnemyPatch
    {
        static void Postfix(MobileParty __instance, float ____attackInitiative, float ____avoidInitiative, MobileParty enemyParty, ref float avoidScore, ref float attackScore)
        {
            if (__instance?.LeaderHero?.getOrder()?.Behavior != null && !__instance.IsJoiningArmy)
            {
                if (enemyParty.IsLordParty && enemyParty.LeaderHero != null && enemyParty.LeaderHero.IsNoble) // Counteract ignoring of initiative scores under these conditions in original method
                {
                    attackScore *= ____attackInitiative;
                    avoidScore *= ____avoidInitiative;
                    if (attackScore < 1f)
                    {
                        return;
                    }
                    avoidScore = 0f;
                }
            }
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }

        static bool Prepare()
        {
            return true;
        }
    }

}
