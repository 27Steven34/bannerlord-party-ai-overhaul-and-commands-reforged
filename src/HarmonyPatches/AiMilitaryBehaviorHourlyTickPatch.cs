﻿using HarmonyLib;
using Helpers;
using PartyAIOverhaulCommands.src;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.Actions;
using TaleWorlds.CampaignSystem.Conversation.Tags;
using TaleWorlds.CampaignSystem.SandBox.CampaignBehaviors.AiBehaviors;
using TaleWorlds.CampaignSystem.SandBox.GameComponents;
using TaleWorlds.CampaignSystem.SandBox.GameComponents.Map;
using TaleWorlds.Core;
using TaleWorlds.Library;
using TaleWorlds.MountAndBlade;
namespace PartyAIOverhaulCommands
{
    [HarmonyPatch(typeof(AiMilitaryBehavior), "AiHourlyTick")]
    [HarmonyPatch(new Type[] { typeof(MobileParty), typeof(PartyThinkParams) })]
    public class AiMilitaryBehaviorHourlyTickPatch
    {
        static void Postfix(MobileParty mobileParty, PartyThinkParams p)
        {
            if (mobileParty?.LeaderHero?.getOrder() != null)
            {
                PartyOrder order = mobileParty.LeaderHero.getOrder();

                //float highest = 0f;
                //string order_target_name = "";
                //string order_type_name = "";
                foreach (KeyValuePair<AIBehaviorTuple, float> keyValuePair in p.AIBehaviorScores.ToList())
                {
                    float value = keyValuePair.Value;
                    IMapPoint target = keyValuePair.Key.Party;
                    if (keyValuePair.Key.AiBehavior == AiBehavior.GoToSettlement)
                    {
                        if (!order.LeaveTroopsToGarrisonOtherClans)
                        {
                            value *= getDoNotReplenishGarrisonCorrectionMult(mobileParty, (Settlement)target);
                        }
                        p.AIBehaviorScores[keyValuePair.Key] = value * order.PartyMaintenanceScoreMultiplier;
                    }
                    else if (keyValuePair.Key.AiBehavior == AiBehavior.DefendSettlement || keyValuePair.Key.AiBehavior == AiBehavior.PatrolAroundPoint)
                    {
                        if (((Settlement)keyValuePair.Key.Party).OwnerClan == mobileParty.LeaderHero.Clan)
                        {
                            p.AIBehaviorScores[keyValuePair.Key] = value * order.OwnClanVillagesScoreMultiplier;
                        }
                        else
                        {
                            p.AIBehaviorScores[keyValuePair.Key] = value * order.FriendlyVillagesScoreMultiplier;
                        }
                    }
                    else if (keyValuePair.Key.AiBehavior == AiBehavior.BesiegeSettlement || keyValuePair.Key.AiBehavior == AiBehavior.AssaultSettlement)
                    {
                        p.AIBehaviorScores[keyValuePair.Key] = value * order.HostileSettlementsScoreMultiplier;
                    }
                    else if (keyValuePair.Key.AiBehavior == AiBehavior.RaidSettlement)
                    {
                        if (!order.AllowRaidingVillages)
                        {
                            p.AIBehaviorScores[keyValuePair.Key] = 0f;
                        }
                    }
                    else if (keyValuePair.Key.AiBehavior == AiBehavior.EngageParty)
                    {
                        InformationManager.DisplayMessage(new InformationMessage("EngageParty: " + keyValuePair.Key.Party.Name + " " + keyValuePair.Value));
                    }

                    //if (p.AIBehaviorScores[keyValuePair.Key] > highest)
                    //{
                    //    highest = p.AIBehaviorScores[keyValuePair.Key];
                    //    order_target_name = keyValuePair.Key.Party.ToString();
                    //    order_type_name = keyValuePair.Key.AiBehavior.ToString();
                    //}
                }
                //InformationManager.DisplayMessage(new InformationMessage("AiMilitaryBehavior: " + p.AIBehaviorScores.Count.ToString()));
                if (mobileParty.IsDisbanding)
                {
                    mobileParty.LeaderHero.cancelOrder();
                }
                else if (order.Behavior == AiBehavior.None)
                {

                }
                else if (mobileParty.Army != null && mobileParty.Army.LeaderParty == Hero.MainHero.PartyBelongedTo)
                {
                    // Army summons from player cancels order except free roam with additonal instructions
                    mobileParty.LeaderHero.cancelOrder();
                }
                else if (order.Behavior == AiBehavior.PatrolAroundPoint)
                {
                    if (order.TargetSettlement == null)
                    {
                        MessageBox.Show("Patrol target settlement not set, please report this bug to the developer of Party Ai Overhaul.");
                    }
                    AIBehaviorTuple aibehaviorTuple = new AIBehaviorTuple(order.TargetSettlement, order.Behavior, false);
                    if (p.AIBehaviorScores.ContainsKey(aibehaviorTuple))
                    {
                        p.AIBehaviorScores[aibehaviorTuple] = order.getScore(p.AIBehaviorScores[aibehaviorTuple]);
                        //InformationManager.DisplayMessage(new InformationMessage(order_type_name + order_target_name + ": " + highest.ToString() + "\nCurrent order: " + order.getScore(p.AIBehaviorScores[aibehaviorTuple]).ToString()));
                    }
                    else
                    {
                        p.AIBehaviorScores.Add(aibehaviorTuple, order.getScore());
                        //InformationManager.DisplayMessage(new InformationMessage(order_type_name + order_target_name + ": " + highest.ToString() + "\nCurrent order: " + order.getScore().ToString()));
                    }
                }
                else if (order.Behavior == AiBehavior.EscortParty) // && (mobileParty.Army == null || mobileParty.Army.LeaderParty != order.TargetParty)) //Extra conditions to not clash with Army muster by same leader
                {
                    AIBehaviorTuple aibehaviorTuple = new AIBehaviorTuple(order.TargetParty, order.Behavior, false);
                    if (p.AIBehaviorScores.ContainsKey(aibehaviorTuple))
                    {
                        p.AIBehaviorScores[aibehaviorTuple] = order.getScore(p.AIBehaviorScores[aibehaviorTuple]);
                        //InformationManager.DisplayMessage(new InformationMessage(order_type_name + order_target_name + ": " + highest.ToString() + "\nCurrent order: " + order.getScore(p.AIBehaviorScores[aibehaviorTuple]).ToString()));
                    }
                    else
                    {
                        p.AIBehaviorScores.Add(aibehaviorTuple, order.getScore());
                        //InformationManager.DisplayMessage(new InformationMessage(order_type_name + order_target_name + ": " + highest.ToString() + "\nCurrent order: " + order.getScore().ToString()));
                    }
                    if (order.ScoreMinimum > 1f && order.TargetParty == Hero.MainHero.PartyBelongedTo && mobileParty.GetNumDaysForFoodToLast() < 3)
                    {
                        InformationManager.DisplayMessage(new InformationMessage(mobileParty.Name + " is short on food.", Colors.Red));
                    }
                }

            }
        }

        public static float getDoNotReplenishGarrisonCorrectionMult(MobileParty mobileParty, Settlement settlement)
        {
            if (settlement.IsVillage || settlement.OwnerClan.Kingdom != mobileParty.LeaderHero.Clan.Kingdom)
                return 1.0f;
            float num21 = FactionHelper.FindIdealGarrisonStrengthPerWalledCenter(mobileParty.MapFaction as Kingdom, null);
            if (mobileParty.Army != null)
            {
                num21 *= 0.75f;
            }
            if (settlement.IsFortification && settlement.OwnerClan != Clan.PlayerClan)
            {
                float garrionstrength = (settlement.Town.GarrisonParty != null) ? settlement.Town.GarrisonParty.Party.TotalStrength : 0f;
                float num70 = FactionHelper.OwnerClanEconomyEffectOnGarrisonSizeConstant(settlement.OwnerClan);
                float num71 = FactionHelper.SettlementProsperityEffectOnGarrisonSizeConstant(settlement);
                float num72 = FactionHelper.SettlementFoodPotentialEffectOnGarrisonSizeConstant(settlement);
                float garrisontarget = num21 * num70 * num71 * num72;
                if (garrionstrength < garrisontarget)
                {
                    return 1 / (1 + (float)Math.Pow((1f - garrionstrength / garrisontarget), 3.0) * 99f);
                }
            }
            return 1.0f;
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }

        static bool Prepare()
        {
            return true;
        }
    }

    // Changes siege targets to closest fortifications to army leader's hometown
    [HarmonyPatch(typeof(AiMilitaryBehavior), "FindBestTargetAndItsValueForFaction")]
    public class FindBestTargetAndItsValueForFactionPatch
    {
        static void Postfix(Army.ArmyTypes missionType, PartyThinkParams p)
        {
            if (missionType == Army.ArmyTypes.Besieger)
            {
                MobileParty party = p?.MobilePartyOf;
                if (party?.Army?.LeaderParty == party && party?.LeaderHero?.Clan?.Kingdom != null)
                {
                    Dictionary<AIBehaviorTuple, float> targets = new Dictionary<AIBehaviorTuple, float>(10);
                    //float total_distance = 0f;
                    float closest_distance = 99999f;
                    Settlement home = party.LeaderHero.HomeSettlement;
                    if (home == null)
                        home = party.LastVisitedSettlement;
                    if (home == null)
                        return;
                    Settlement closest_target = null;
#if DEBUG
                    float closest_old_score = 0f;
                    float highest_old_score = 0f;
                    Settlement highest_old_settlement = null;
#endif
                    foreach (KeyValuePair<AIBehaviorTuple, float> pair in p.AIBehaviorScores)
                    {
                        if (pair.Value > 0 && pair.Key.AiBehavior == AiBehavior.BesiegeSettlement && pair.Key.Party != null && pair.Key.Party as Settlement != null)
                        {
                            targets.Add(pair.Key, pair.Value);
                            float distance = Campaign.Current.Models.MapDistanceModel.GetDistance(home, pair.Key.Party as Settlement); 
                            if (distance < closest_distance)
                            {
#if DEBUG
                                closest_old_score = pair.Value;
#endif
                                closest_distance = distance;
                                closest_target = pair.Key.Party as Settlement;
                            }
#if DEBUG
                            if (pair.Value > highest_old_score)
                            {
                                highest_old_score = pair.Value;
                                highest_old_settlement = pair.Key.Party as Settlement;
                            }
#endif
                        }
                    }
#if DEBUG
                    float highest_score = 0f;
                    Settlement highest_settlement = null;
                    float new_old_settlement = 0f;
#endif
                    foreach (KeyValuePair<AIBehaviorTuple, float> pair in targets)
                    {
                        Settlement target = pair.Key.Party as Settlement;
                        float distance = Campaign.Current.Models.MapDistanceModel.GetDistance(home, target);
                        float new_score = pair.Value * 1.2f * Math.Max(0, (1 - ((distance - closest_distance) / (
                            target.Culture == party.LeaderHero.Culture && closest_target.Culture != party.LeaderHero.Culture ? Campaign.AverageDistanceBetweenTwoTowns : Campaign.AverageDistanceBetweenTwoTowns / 3))));
                        p.AIBehaviorScores[pair.Key] = new_score;
#if DEBUG
                        if (new_score > highest_score)
                        {
                            highest_score = new_score;
                            highest_settlement = pair.Key.Party as Settlement;
                        }
                        if (pair.Key.Party as Settlement == highest_old_settlement)
                            new_old_settlement = new_score;
#endif
                    }
#if DEBUG
                    if (closest_target != highest_settlement)
                        InformationManager.DisplayMessage(new InformationMessage(party.Name + " Old Best: " + highest_old_settlement.Name + " " + highest_old_score + "\nClosest: " + closest_target.Name + " " + closest_old_score + "\nHighest: " + highest_settlement.Name + " " + highest_score, Colors.Red));
                    if (highest_settlement != highest_old_settlement)
                        if (highest_score < 1f && highest_old_score > 1f)
                            InformationManager.DisplayMessage(new InformationMessage(party.Name + " Old Best: " + highest_old_settlement.Name + " " + highest_old_score + "\nNew Old: " + highest_old_settlement.Name + " " + new_old_settlement + "\nHighest: " + highest_settlement.Name + " " + highest_score, Colors.Yellow));
                        else
                            InformationManager.DisplayMessage(new InformationMessage(party.Name + " Old Best: " + highest_old_settlement.Name + " " + highest_old_score + "\nNew Old: " + highest_old_settlement.Name + " " + new_old_settlement + "\nHighest: " + highest_settlement.Name + " " + highest_score, Colors.Green));

#endif
                    //float average_distance = total_distance / targets.Count();
                }
            }
        }
        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }

        static bool Prepare()
        {
            return Config.Value.EnableBorderOnlySieges;
        }
    }
}
